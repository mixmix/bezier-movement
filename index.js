// https://gamedev.stackexchange.com/questions/27056/how-to-achieve-uniform-speed-of-movement-on-a-bezier-curve

const root = document.getElementById('app')
const canvas = document.createElement('canvas')

const WIDTH = 600
const HEIGHT = 600
const PADDING = 50

canvas.width = WIDTH
canvas.height = HEIGHT
canvas.style = `width: ${WIDTH}px; border: 1px solid black; margin: 1rem;`

root.appendChild(canvas)

const ctx = canvas.getContext('2d')

// A collection of bezier curves:
//   Curve = [start, CP1, CP2, end]
// They are expected to be continuous and smooth

function transform (coord) {
  coord.x = coord.x + WIDTH / 2
  coord.y = coord.y + HEIGHT / 2
  return coord
}

const curves = [
  // from top down to split
  [
    { x: 0, y: -HEIGHT / 2 + PADDING },
    { x: 100, y: -HEIGHT / 2 + PADDING },
    { x: WIDTH / 3 - PADDING, y: -200 },
    { x: WIDTH / 3 - PADDING, y: -HEIGHT / 4 }
  ],

  // left split
  [
    { x: WIDTH / 3 - PADDING, y: -HEIGHT / 4 },
    { x: WIDTH / 3 - PADDING, y: -HEIGHT / 4 + 100 },
    { x: WIDTH / 3 - PADDING - 60, y: -100 },
    { x: WIDTH / 3 - PADDING - 60, y: 0 }
  ],
  [
    { x: WIDTH / 3 - PADDING - 60, y: 0 },
    { x: WIDTH / 3 - PADDING - 60, y: 100 },
    { x: WIDTH / 3 - PADDING, y: HEIGHT / 4 - 100 },
    { x: WIDTH / 3 - PADDING, y: HEIGHT / 4 }
  ],

  // left-inner split
  [
    { x: WIDTH / 3 - PADDING, y: -HEIGHT / 4 },
    { x: WIDTH / 3 - PADDING, y: -HEIGHT / 4 + 100 },
    { x: WIDTH / 3 - PADDING - 20, y: -100 },
    { x: WIDTH / 3 - PADDING - 20, y: 0 }
  ],
  [
    { x: WIDTH / 3 - PADDING - 20, y: 0 },
    { x: WIDTH / 3 - PADDING - 20, y: 100 },
    { x: WIDTH / 3 - PADDING, y: HEIGHT / 4 - 100 },
    { x: WIDTH / 3 - PADDING, y: HEIGHT / 4 }
  ],

  // right-inner split
  [
    { x: WIDTH / 3 - PADDING, y: -HEIGHT / 4 },
    { x: WIDTH / 3 - PADDING, y: -HEIGHT / 4 + 100 },
    { x: WIDTH / 3 - PADDING + 20, y: -100 },
    { x: WIDTH / 3 - PADDING + 20, y: 0 }
  ],
  [
    { x: WIDTH / 3 - PADDING + 20, y: 0 },
    { x: WIDTH / 3 - PADDING + 20, y: 100 },
    { x: WIDTH / 3 - PADDING, y: HEIGHT / 4 - 100 },
    { x: WIDTH / 3 - PADDING, y: HEIGHT / 4 }
  ],

  // right split
  [
    { x: WIDTH / 3 - PADDING, y: -HEIGHT / 4 },
    { x: WIDTH / 3 - PADDING, y: -HEIGHT / 4 + 100 },
    { x: WIDTH / 3 - PADDING + 60, y: -100 },
    { x: WIDTH / 3 - PADDING + 60, y: 0 }
  ],
  [
    { x: WIDTH / 3 - PADDING + 60, y: 0 },
    { x: WIDTH / 3 - PADDING + 60, y: 100 },
    { x: WIDTH / 3 - PADDING, y: HEIGHT / 4 - 100 },
    { x: WIDTH / 3 - PADDING, y: HEIGHT / 4 }
  ],

  // from split down to bottom
  [
    { x: WIDTH / 3 - PADDING, y: HEIGHT / 4 },
    { x: WIDTH / 3 - PADDING, y: +200 },
    { x: 100, y: HEIGHT / 2 - PADDING },
    { x: 0, y: HEIGHT / 2 - PADDING }
  ],

  // from bottom to top again
  [
    { x: 0, y: HEIGHT / 2 - PADDING },
    { x: -100, y: HEIGHT / 2 - PADDING },
    { x: -WIDTH / 3 + PADDING, y: +200 },
    { x: -WIDTH / 3 + PADDING, y: 0 }
  ],
  [
    { x: -WIDTH / 3 + PADDING, y: 0 },
    { x: -WIDTH / 3 + PADDING, y: -200 },
    { x: -100, y: -HEIGHT / 2 + PADDING },
    { x: 0, y: -HEIGHT / 2 + PADDING }
  ]
].map(points => points.map(transform))

// build a map which allows us to know which curve(s) follow a particular curve
const nextCurveMap = curves.reduce((acc, curve, i) => {
  acc[i] = curves.reduce((acc, nextCurve, j) => {
    if (
      nextCurve[0].x === curve[3].x &&
      nextCurve[0].y === curve[3].y
    ) acc.push(j)

    return acc
  }, [])
  if (acc[i].length === 0) throw new Error('missing connection for curve ' + i)

  return acc
}, {})
function getNextCurveIndex (i) {
  return nextCurveMap[i]
    .sort((a, b) => Math.random() > 0.5 ? -1 : +1)[0]
}

const Vector = {
  add (...vectors) {
    return vectors.reduce((acc, vector) => {
      acc.x += vector.x
      acc.y += vector.y
      return acc
    }, { x: 0, y: 0 })
  },
  subtract (b, a) {
    return {
      x: b.x - a.x,
      y: b.y - a.y
    }
  },
  scalarMult (k, a) {
    return {
      x: k * a.x,
      y: k * a.y
    }
  }
}

function buildBezierPoint (A, B, C, D) {
  // For t (0,1), we can find the point on a bezier curve using a function which
  // is a combination of the vectors A,B,C,D
  // We can do some maths and pre-compute some vectors v1, v2, v3, which saves
  // some computation!

  // B(t) = (1−t)3A + 3t(1−t)2B + 3t2(1−t)C + t3D
  //      = t**3 * (−A+3B−3C+D) + t**2 * (3A−6B+3C) + t * (−3A+3B) + A
  //      = t**3 * v1           + t**2 * v2         + t * v3       + A

  const v1 = Vector.add(
    Vector.scalarMult(-1, A),
    Vector.scalarMult(3, B),
    Vector.scalarMult(-3, C),
    D
  )

  const v2 = Vector.add(
    Vector.scalarMult(3, A),
    Vector.scalarMult(-6, B),
    Vector.scalarMult(3, C)
  )

  const v3 = Vector.add(
    Vector.scalarMult(-3, A),
    Vector.scalarMult(3, B)
  )

  return function bezierPoint (t) {
    return Vector.add(
      Vector.scalarMult(t ** 3, v1),
      Vector.scalarMult(t ** 2, v2),
      Vector.scalarMult(t, v3),
      A
    )
  }
}

// Cubic Bézier curve
function drawPath (A, B, C, D, color) {
  ctx.strokeStyle = color || 'black'
  ctx.lineWidth = 10
  ctx.beginPath()
  ctx.moveTo(A.x, A.y)
  ctx.bezierCurveTo(B.x, B.y, C.x, C.y, D.x, D.y)
  ctx.stroke()
}

function drawPoint (vector, color) {
  ctx.strokeStyle = color || 'black'
  ctx.lineWidth = 1
  ctx.beginPath()
  ctx.arc(vector.x, vector.y, 2, 0, 2 * Math.PI)
  ctx.stroke()
}

const frameRate = 30
const period = 1 / frameRate
const Δt = period / 5 // vaguely some speed

const POINTS = 80

// set up points starting on random curve, with random t
const points = new Array(POINTS).fill(0).map(i => {
  return {
    curveIndex: Math.floor(curves.length * Math.random()),
    t: Math.random()
  }
})

// pre-compute some functions for efficiently drawing points on each curve
const curvesBezierPoints = curves.map(points => buildBezierPoint(...points))

let coords

// Draw loop!
const stepAnimation = () => {
  // wipe screen
  ctx.fillStyle = '#222'
  ctx.fillRect(0, 0, canvas.width, canvas.height)

  curves.forEach(curve => {
    drawPath(...curve, '#303')
  })

  points.forEach(point => {
    coords = curvesBezierPoints[point.curveIndex](point.t)
    drawPoint(coords, '#f34')

    // increment the position for next step
    point.t = point.t + Δt
    // if has run over the end of this curve, place it on the next curve
    if (point.t > 1) {
      point.curveIndex = getNextCurveIndex(point.curveIndex)
      point.t = point.t - 1
    }
  })

  requestAnimationFrame(stepAnimation)
}

requestAnimationFrame(stepAnimation)

// NEXT UP: add sinusoidal oscilation around path
//   - calculate the normal to the curve (take derivative again)
//   - normalise this, then add a vector to bezier position:
//      unitNormal * sin(kt)

// NEXT UP: implement approx constant movement as described in link
// v1 = −3A+9B−9C+3D
// v2 = 6A−12B+6C
// v3 = −3A+3B
//
// Δt = L / length(t**2 * v1 + t * v2 * + v3)
